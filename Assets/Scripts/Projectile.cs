﻿using UnityEngine;
using System.Collections;

public class Projectile : HitBox {

	protected override void OnTriggerEnter2D(Collider2D other) {
		if(other.GetComponent<Pokemon>()!=null && !other.GetComponent<Pokemon>().Equals(pokemon)) {
			onHit(other.gameObject.GetComponent<Pokemon>());
			Destroy();
		}
	}

	public void Destroy() {
		Object.Destroy(gameObject);
	}

}
