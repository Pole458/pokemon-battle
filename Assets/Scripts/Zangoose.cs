﻿using UnityEngine;
using System.Collections;

public class Zangoose : Pokemon {
	
	protected override void Start() {
		base.Start();
	}

	protected override void Update () {

		base.Update();

		switch(status) {
		case "":
			if(grounded) {
				
				if(Input.GetButtonDown(Move2)) {
					if(Input.GetAxisRaw(Vertical)==-1) {
						//HoneClawsChannel
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetBool("HoneClawsChannel",true);
						setStatus("HoneClawsChannel");
						break;
					}
					if(Input.GetAxisRaw(Horizontal)==0) {
						//DashAttackChannel
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetBool("DashAttackChannel",true);
						setStatus("DashAttackChannel");
						break;
					}
				}
				
				if(Input.GetButtonDown(Move1)) {
					if(Input.GetAxisRaw(Vertical)==-1) {
						//Guard
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetBool("Guard", true);
						setStatus("Guard");
						break;
					} else {
						//Punch1
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetTrigger("Punch1");
						setStatus("Punch1");
						break;
					}
				}
				
			} else {
				//Fall
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Fall")) {
					setStatus("MidAir");
					break;
				}
			}
			
			//Jump
			if(Input.GetAxisRaw(Vertical)==1 && canJump) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,9);
				GetComponent<Animator>().SetTrigger("Jump");
				setStatus("MidAir");
				canJump = false;
				break;
			}
		
			checkDirection();
			hInput();
			
			break;
		case "MidAir":
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,Mathf.Max(GetComponent<Rigidbody2D>().velocity.y,-4));
			//JumpAttack
			if(Input.GetButtonDown(Move1) && canDahsMidAir) {
				if(faceRight) GetComponent<Rigidbody2D>().velocity = new Vector2(8,0);
				else GetComponent<Rigidbody2D>().velocity = new Vector2(-8,0);
				GetComponent<Animator>().SetTrigger("JumpAttack");
				setStatus("JumpAttack");
				canJump = false;
				canDahsMidAir = false;
				break;
			}

			//Jump
			if(Input.GetAxisRaw(Vertical)==1 && canJump) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,9);
				GetComponent<Animator>().SetTrigger("Jump");
				canJump = false;
				break;
			}
			//Dive
			if(Input.GetAxisRaw(Vertical)==-1) {
				GetComponent<Animator>().SetTrigger("Dive");
				setStatus("Dive");
				canJump = false;
				canDahsMidAir = false;
				break;
			}
			//Idle || Walk
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle") || GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Walk")) {
				setStatus("");
				break;
			}
			//"Stop" Jumping
			if(Input.GetAxisRaw(Vertical)==0 && GetComponent<Rigidbody2D>().velocity.y>5 ) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,5);
			}
			hInput();
			checkDirection();
			break;
		case "Dive":
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,-4);
			
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Dive")) {
				setStatus("Land");
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				break;
			}
			
			hInput();
			checkDirection();
			break;
		case "Land":
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Land")) {
				setStatus("");
				break;
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "Guard":
			if(!Input.GetButton(Move1)) {
				GetComponent<Animator>().SetBool("Guard", false);
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				setStatus("");
				break;
			}
			InvertedCheckDirection();
			GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.MoveTowards(GetComponent<Rigidbody2D>().velocity.x,0,8*Time.deltaTime),GetComponent<Rigidbody2D>().velocity.y);
			break;
		case "Hit":
			GetComponent<Animator>().SetBool("HoneClawsChannel",false);

			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Hit")) {
				setStatus("");
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				break;
			}

			InvertedCheckDirection();
			hitbox.EnableCollider(false);
			break;
		case "Punch1":
			//Get input for the Punch2
			if(Input.GetButtonDown(Move1)) {
				GetComponent<Animator>().SetTrigger("Punch2");
			}
			
			//Punch2
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
				setStatus("Punch2");
				break;
			}
			//""
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch1")) {
				setStatus("");
				GetComponent<Animator>().ResetTrigger("Punch2");
				break;
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "Punch2":
			//Get input for...
			if(Input.GetButtonDown(Move1)) {
				//...LowAttack
				if(Input.GetAxisRaw(Vertical)==-1) {
					GetComponent<Animator>().SetTrigger("LowAttack");
				} else {
					if(Input.GetAxisRaw(Horizontal)==0) {
						//...Punch1
						GetComponent<Animator>().SetTrigger("Punch1");
					} else {
						//...ShoudlerBash
						if( faceRight == (Input.GetAxisRaw(Horizontal)==1) )
							GetComponent<Animator>().SetTrigger("ShoulderBash");
					} 
				}
			}
			
			//On exit-time
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
				//LowAttack
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LowAttack")) {
					//Low Attack
					setStatus("LowAttack");
					GetComponent<Animator>().ResetTrigger("ShoulderBash");
					GetComponent<Animator>().ResetTrigger("Punch1");
					break;
				}
				//ShoulderBash
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("ShoulderBash")) {
					GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x*6,0);
					setStatus("ShoulderBash");
					GetComponent<Animator>().ResetTrigger("Punch1");
					GetComponent<Animator>().ResetTrigger("LowAttack");
					break;
				}
				//Punch1
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch1")) {
					setStatus("Punch1");
					GetComponent<Animator>().ResetTrigger("ShoulderBash");
					GetComponent<Animator>().ResetTrigger("LowAttack");
					break;	
				}
				//""
				if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
					setStatus("");
					GetComponent<Animator>().ResetTrigger("ShoulderBash");
					GetComponent<Animator>().ResetTrigger("LowAttack");
					GetComponent<Animator>().ResetTrigger("Punch1");
					break;	
				}
			}
			
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "LowAttack":
			//""
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
				setStatus("");
				break;	
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "ShoulderBash":
			//""
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				setStatus("");
				break;	
			}

			break;
		case "JumpAttack":
			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("JumpAttack")) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(0,-4);
				setStatus("Dive");
				break;	
			}
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,0);
			break;
		case "DashAttackChannel":
			//Get input for DashAttack
			if(!Input.GetButton(Move2)) {
				GetComponent<Animator>().SetBool("DashAttackChannel",false);
			}
			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("DashAttackChannel")) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x*8,0);
				GetComponent<Animator>().SetBool("DashAttackChannel",false);
				setStatus("DashAttack");
				break;
			}
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "DashAttack":
			//Idle
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				setStatus("");
				canJump = false;
				break;	
			}

			break;
		case "HoneClawsChannel":
			//Get input for HoneClaws
			if(!Input.GetButton(Move2)) {
				GetComponent<Animator>().SetBool("HoneClawsChannel",false);
			}
			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("HoneClawsChannel")) {
				GetComponent<Animator>().SetBool("HoneClawsChannel",false);
				setStatus("HoneClaws");
				break;
			}
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "HoneClaws":
			//On exit
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
				setStatus("");
				break;
			}

			break;
		}
	}
}
