﻿using UnityEngine;
using System.Collections;

public class Pokemon : MonoBehaviour {

	//Stats
	public float perc;
	public int lifes;

	protected float deathTimer;

	//Movement
	protected float speed;
	protected bool faceRight;
	public string status;
	protected bool canJump;
	protected bool canDahsMidAir; //superflua
	protected bool grounded;

	//Inputs
	protected string Horizontal;
	protected string Vertical;
	protected string Move1;
	protected string Move2;

	//Components
	public HitBox hitbox;

	protected virtual void Start () {
		perc = 0;
		lifes = 3;

		speed = 2.5f;
		faceRight=true;
		Spawn();
	}

	void Spawn() {
		setStatus("");
		canJump=false;
		canDahsMidAir = false;
		GetComponent<Animator>().enabled = true;
		GetComponent<SpriteRenderer>().enabled = true;
		GetComponent<BoxCollider2D>().enabled = true;
		GetComponent<Rigidbody2D>().isKinematic = false;
	}

	protected virtual void Update () {

		if(status == "Dead" ) {

			if(Time.time>deathTimer && lifes>0) Spawn();
		
		} else {

			grounded = IsGrounded();
			
			if(grounded) {
				canJump = true;
				canDahsMidAir = true;
			}
			
			GetComponent<Animator>().SetBool("Grounded",grounded);
			
			GetComponent<Animator>().SetInteger("hSpeed",(int)Mathf.Round(GetComponent<Rigidbody2D>().velocity.x*10f));
			
			GetComponent<Animator>().SetInteger("vSpeed",(int)Mathf.Round(GetComponent<Rigidbody2D>().velocity.y*10f));
		}
	}

	protected void checkDirection() {
		if(GetComponent<Rigidbody2D>().velocity.x>0 && !faceRight) {
			Flip();
			faceRight=true;
		} else if(GetComponent<Rigidbody2D>().velocity.x<0 && faceRight) {
			Flip();
			faceRight=false;
		}
	}

	public void InvertedCheckDirection() {
		if(GetComponent<Rigidbody2D>().velocity.x>0 && faceRight) {
			Flip();
			faceRight = false;
		} else if(GetComponent<Rigidbody2D>().velocity.x<0 && !faceRight) {
			Flip();
			faceRight = true;
		}
	}


	public virtual void Death() {
		lifes--;
		perc = 0;
		GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		transform.position = Vector2.zero;
		GetComponent<Animator>().enabled = false;
		GetComponent<SpriteRenderer>().color = Color.white;
		GetComponent<SpriteRenderer>().enabled = false;
		GetComponent<BoxCollider2D>().enabled = false;
		GetComponent<Rigidbody2D>().isKinematic = true;
		hitbox.EnableCollider(false);
		deathTimer = Time.time + 3;
		setStatus("Dead");
	}

	public void Flip() {
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	protected void hInput() {
		if(Input.GetAxisRaw(Horizontal)!=0)
			GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxisRaw(Horizontal)*speed,GetComponent<Rigidbody2D>().velocity.y);
		else 
			GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.MoveTowards(GetComponent<Rigidbody2D>().velocity.x,0,8*Time.deltaTime),GetComponent<Rigidbody2D>().velocity.y);
	}

	protected bool IsGrounded() {
		return Physics2D.Raycast((Vector2)transform.position + new Vector2(-GetComponent<BoxCollider2D>().bounds.extents.x+0.1f,GetComponent<BoxCollider2D>().offset.y-GetComponent<BoxCollider2D>().bounds.extents.y-0.05f), -transform.up, 0.05f, 1 << LayerMask.NameToLayer("Wall"))
			|| Physics2D.Raycast((Vector2)transform.position + new Vector2(GetComponent<BoxCollider2D>().bounds.extents.x-0.1f,GetComponent<BoxCollider2D>().offset.y-GetComponent<BoxCollider2D>().bounds.extents.y-0.05f), -transform.up, 0.05f, 1 << LayerMask.NameToLayer("Wall"));
	}

	//Getters & Setters*************************************************************************************************
	public void setButtons(int player) {
		switch(player) {
		case 0:
			Horizontal = "Horizontal_1";
			Vertical = "Vertical_1";
			Move1 = "Fire1_1";
			Move2 = "Fire2_1";
				break;
		case 1:
			Horizontal = "Horizontal_2";
			Vertical = "Vertical_2";
			Move1 = "Fire1_2";
			Move2 = "Fire2_2";
			break;
		}
	}
	public void setStatus(string s) {
		status = s;
	}
	public bool isFacingRight() {
		return faceRight;
	}
	public void setFaceRight(bool b) {
		faceRight = b;
	}
	public string getStatus() {
		return status;
	}
	//Getters & Setters*************************************************************************************************






}