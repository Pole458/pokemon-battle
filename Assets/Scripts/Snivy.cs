﻿using UnityEngine;
using System.Collections;

public class Snivy : Pokemon {

	public Projectile razorLeaf;

	public Projectile solarBeam1;
	public Projectile solarBeam2;
	public Projectile solarBeam3;
	
	protected bool razorLeafProc;
	protected bool solarBeamProc;


	protected override void Start() {
		base.Start();

		razorLeafProc = false;
		solarBeamProc = false;
	}

	protected override void Update () {
		
		base.Update();
		
		switch(status) {
		case "":
			if(grounded) {
				
				if(Input.GetButtonDown(Move2)) {
					//SolarBeamChannel
					if(Input.GetAxisRaw(Vertical)==-1) {
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetBool("SolarBeamChannel",true);
						setStatus("SolarBeamChannel");
						break;
					} else {
						//RazorLeaf
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetBool("RazorLeaf", true);
						setStatus("RazorLeaf");
						break;
					}
				}

				if(Input.GetButtonDown(Move1)) {
					if(Input.GetAxisRaw(Vertical)==-1) {
						//Guard
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetBool("Guard", true);
						setStatus("Guard");
						break;
					} else {
						//Punch1
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetTrigger("Punch1");
						setStatus("Punch1");
						break;
					}
				}
				
			} else {
				//Fall
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Fall")) {
					setStatus("MidAir");
					break;
				}
			}
			//Jump
			if(Input.GetAxisRaw(Vertical)==1 && canJump) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,9);
				GetComponent<Animator>().SetTrigger("Jump");
				setStatus("MidAir");
				canJump = false;
				break;
			}
			checkDirection();
			hInput();
			break;
		case "MidAir":
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,Mathf.Max(GetComponent<Rigidbody2D>().velocity.y,-4));
			//LeafTornado
			if(Input.GetButtonDown(Move2) && canDahsMidAir) {
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				GetComponent<Animator>().SetTrigger("LeafTornado");
				setStatus("LeafTornado");
				canJump = false;
				canDahsMidAir = false;
				break;
			}
			//Vinewhip
			if(Input.GetButtonDown(Move1) && canDahsMidAir) {
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				GetComponent<Animator>().SetTrigger("Vinewhip");
				setStatus("Vinewhip");
				canJump = false;
				canDahsMidAir = false;
				GetComponent<Rigidbody2D>().isKinematic = true;
				break;
			}
			//Jump
			if(Input.GetAxisRaw(Vertical)==1 && canJump) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,9);
				GetComponent<Animator>().SetTrigger("Jump");
				canJump = false;
				break;
			}
			//Dive
			if(Input.GetAxisRaw(Vertical)==-1) {
				GetComponent<Animator>().SetTrigger("Dive");
				setStatus("Dive");
				canJump = false;
				canDahsMidAir = false;
				break;
			}
			//Idle || Work
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle") || GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Walk")) {
				setStatus("");
				break;
			}
			//"Stop" Jumping
			if(Input.GetAxisRaw(Vertical)==0 && GetComponent<Rigidbody2D>().velocity.y>5 ) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,5);
			}
			hInput();
			checkDirection();
			break;
		case "Dive":
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,-4);
			
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Dive")) {
				setStatus("Land");
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				break;
			}
			
			hInput();
			checkDirection();
			break;
		case "Land":
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Land")) {
				setStatus("");
				break;
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "Guard":
			if(!Input.GetButton(Move1)) {
				GetComponent<Animator>().SetBool("Guard", false);
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				setStatus("");
				break;
			}
			InvertedCheckDirection();
			GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.MoveTowards(GetComponent<Rigidbody2D>().velocity.x,0,8*Time.deltaTime),GetComponent<Rigidbody2D>().velocity.y);
			break;
		case "Hit":
			GetComponent<Animator>().SetBool("SolarBeamChannel",false);

			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Hit")) {
				setStatus("");
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				break;
			}

			InvertedCheckDirection();
			hitbox.EnableCollider(false);
			break;
		case "Punch1":
			//Get input for the Punch2
			if(Input.GetButtonDown(Move1)) {
				GetComponent<Animator>().SetTrigger("Punch2");
			}
			
			//Punch2
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
				setStatus("Punch2");
				break;
			}
			//""
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch1")) {
				setStatus("");
				GetComponent<Animator>().ResetTrigger("Punch2");
				break;
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "Punch2":
			//Get input for...
			if(Input.GetButtonDown(Move1)) {
				//...Vinewhip
				if(Input.GetAxisRaw(Horizontal)!=0) {
					GetComponent<Animator>().SetTrigger("Vinewhip");
				} else {
					//... Punch1
					GetComponent<Animator>().SetTrigger("Punch1");
				}
			}

			//On exit-time
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
				//Vine whip
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Vinewhip")) {
					setStatus("Vinewhip");
					GetComponent<Animator>().ResetTrigger("Punch1");
					break;
				}
				//Punch1
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch1")) {
					setStatus("Punch1");
					GetComponent<Animator>().ResetTrigger("Vinewhip");
					break;
				}
				//""
				if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
					setStatus("");
					GetComponent<Animator>().ResetTrigger("Punch1");
					GetComponent<Animator>().ResetTrigger("Vinewhip");
					break;
				}
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "Vinewhip":
			//Dive
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Dive") ) {
				setStatus("Dive");
				GetComponent<Rigidbody2D>().isKinematic = false;
				break;	
			}
			//Idle
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle") ) {
				setStatus("");
				break;	
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "RazorLeaf":
			if(GetComponent<SpriteRenderer>().sprite.name == "Snivy_RazorLeaf_2" && !razorLeafProc) {
				Projectile r = (Projectile)Instantiate(razorLeaf, transform.position + new Vector3(1.2f*transform.localScale.x,+0.075f,0) , Quaternion.identity);
				r.pokemon = this;
				r.setHit(5,1,1);
				r.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * 6,0);
				razorLeafProc = true;
			}

			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("RazorLeaf")) {
				setStatus("");
				razorLeafProc = false;
				break;	
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "LeafTornado":

			//Dive
			if(Input.GetAxisRaw(Vertical)==-1) {
				GetComponent<Animator>().SetTrigger("Dive");
				setStatus("Dive");
				canJump = false;
				canDahsMidAir = false;
				break;
			}

			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LeafTornado")) {
				setStatus("Dive");
				canJump = false;
				canDahsMidAir = false;
				break;
			}

			//Idle
			if(IsGrounded()) {
				setStatus("");
				break;
			}

			//Spamming the move2 will make you fly
			if(Input.GetButtonDown(Move2))
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 4);

			hInput();
			break;
		case "SolarBeamChannel":
			//Get input for SolarBeam
			if(!Input.GetButton(Move2)) {
				GetComponent<Animator>().SetBool("SolarBeamChannel",false);
			}
			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("SolarBeamChannel")) {
				setStatus("SolarBeam");
				GetComponent<Animator>().SetBool("SolarBeamChannel",false);
				break;
			}
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "SolarBeam":
			//On exit
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
				setStatus("");
				solarBeamProc = false;
				break;
			}

			//SOLARBEAM
			if(!solarBeamProc && GetComponent<SpriteRenderer>().sprite.name == "Snivy_SolarBeam_3") {
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("SolarBeam1")) {
					Projectile r = (Projectile)Instantiate(solarBeam1, transform.position + new Vector3(1.2f*transform.localScale.x,+0.075f,0), Quaternion.identity);
					r.pokemon = this;
					r.setHit(8,2,2);
					r.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * 6,0);
					solarBeamProc = true;
				} else if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("SolarBeam2")) {
					Projectile r = (Projectile)Instantiate(solarBeam2, transform.position + new Vector3(1.2f*transform.localScale.x,+0.075f,0), Quaternion.identity);
					r.pokemon = this;
					r.setHit(15,2.5f,2.5f);
					r.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * 7,0);
					solarBeamProc = true;
				} else if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("SolarBeam3")) {
					Projectile r = (Projectile)Instantiate(solarBeam3, transform.position + new Vector3(1.2f*transform.localScale.x,+0.25f,0), Quaternion.identity);
					r.pokemon = this;
					r.setHit(25,4,4);
					r.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * 8,0);
					solarBeamProc = true;
				}
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		}
	}
}
