﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	void OnTriggerExit2D(Collider2D other) {

		if(other.GetComponent<Pokemon>()!=null)
			other.GetComponent<Pokemon>().Death();

		if(other.GetComponent<Projectile>()!=null) {
			other.GetComponent<Projectile>().Destroy();
		}

	}

}