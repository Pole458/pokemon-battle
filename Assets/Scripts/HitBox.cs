﻿using UnityEngine;
using System.Collections;

public class HitBox : MonoBehaviour {

	public Pokemon pokemon;
	public float damage;
	public float forceH;
	public float forceV;

	protected virtual void OnTriggerEnter2D(Collider2D other) {
		if(other.GetComponent<Pokemon>()!=null && !other.GetComponent<Pokemon>().Equals(pokemon)) {
			onHit(other.gameObject.GetComponent<Pokemon>());
		}
	}

	protected virtual void onHit(Pokemon p) {

		if(p.getStatus() == "Guard") {
			p.GetComponent<Rigidbody2D>().velocity = new Vector2(pokemon.transform.localScale.x ,p.GetComponent<Rigidbody2D>().velocity.y);
		} else {
			p.perc+=damage;
			p.setStatus("Hit");
			p.GetComponent<Animator>().SetTrigger("Hit");
			p.GetComponent<Animator>().Update(Time.deltaTime);
			p.GetComponent<Animator>().SetFloat("HitTime", calculateHitTime(p));
			p.GetComponent<Rigidbody2D>().velocity = new Vector2(calculateKnockBack(p), calculateKnockUp(p));
		}
	}

	private float calculateHitTime(Pokemon p) {
		return 1/Mathf.Pow(Mathf.Max(Mathf.Abs(forceH),Mathf.Abs(forceV)), 0.5f+p.perc/100 );
	}

	private float calculateKnockBack(Pokemon p) {
		if(GetComponent<Projectile>()!=null) {
			return Mathf.Pow(Mathf.Abs(forceH), 1+p.perc/100) * Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x);
		} else return Mathf.Pow(Mathf.Abs(forceH), 1+p.perc/100) *  pokemon.transform.localScale.x;
	}

	private float calculateKnockUp(Pokemon p) {
		if(forceV==0) return 0;
		if(forceV>0)
			return Mathf.Pow(Mathf.Abs(forceV), 1+p.perc/100);
		else 
			return -Mathf.Pow(Mathf.Abs(forceV), 1+p.perc/100);
	}

	public void EnableCollider(bool b) {
		GetComponent<BoxCollider2D>().enabled = b;
	}

	public bool isColliderEnabled() {
		return GetComponent<BoxCollider2D>().enabled;
	}

	//Getters && Setter
	public void setOffset(float x, float y) {
		GetComponent<BoxCollider2D>().offset = new Vector2(x,y);
	}

	public void setSize(float w, float h) {
		GetComponent<BoxCollider2D>().size = new Vector2(w,h);
	}

	public void setHit(float d, float fH, float fV) {
		damage = d;
		forceH = fH;
		forceV = fV;
	}

}
