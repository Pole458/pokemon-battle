﻿using UnityEngine;
using System.Collections;

public class Lucario : Pokemon {

	public Projectile AuraSphere;

	protected Projectile a;
	protected float aTime;

	protected float pTime;

	protected override void Start() {
		base.Start();
	}

	protected override void Update () {
	
		base.Update();

		if(GetComponent<Animator>().enabled) {
			if(GetComponent<Animator>().GetBool("PowerUp")) {
				GetComponent<Animator>().SetFloat("Power",1.5f);
				speed = 3f;
				if(Time.time-pTime>10) GetComponent<Animator>().SetBool("PowerUp",false);
			} else {
				speed = 2.5f;
				GetComponent<Animator>().SetFloat("Power",1f);
			}
		}

		switch(status) {
		case "":
			if(grounded) {

				if(Input.GetButtonDown(Move2)) {
					if(Input.GetAxisRaw(Vertical)==-1) {
						//AuraSphereChannel
						GetComponent<Animator>().SetBool("AuraSphereChannel",true);
						setStatus("AuraSphereChannel");
						//Create the sphere
						a = (Projectile)Instantiate(AuraSphere, transform.position + new Vector3(-0.45f*transform.localScale.x,0.1f,0), Quaternion.identity);
						a.transform.localScale = new Vector3(0.2f,0.2f,1);
						a.GetComponent<SpriteRenderer>().color = new Color(1,1,1,0.5f);
						aTime = Time.time;
						break;
					} else {
						//PowerUpChannel
						if(!GetComponent<Animator>().GetBool("PowerUp")) {
							GetComponent<Animator>().SetBool("PowerUpChannel",true);
							setStatus("PowerUpChannel");
							pTime = Time.time;
							break;
						}
					}
				}

				if(Input.GetButtonDown(Move1)) {
					if(Input.GetAxisRaw(Vertical)==-1) {
						//Guard
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetBool("Guard", true);
						setStatus("Guard");
						break;
					} else {
						//Punch1
						GetComponent<Rigidbody2D>().velocity = Vector2.zero;
						GetComponent<Animator>().SetTrigger("Punch1");
						setStatus("Punch1");
						break;
					}
				}

				//Jump
				if(Input.GetAxisRaw(Vertical)==1 && canJump) {
					GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,9);
					GetComponent<Animator>().SetTrigger("Jump");
					setStatus("MidAir");
					canJump = false;
					break;
				}

			} else {
				//Fall
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Fall")) {
					setStatus("MidAir");
					break;
				}
			}

			checkDirection();
			hInput();
			break;
		case "MidAir":
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,Mathf.Max(GetComponent<Rigidbody2D>().velocity.y,-4));

			//AirKick
			if(Input.GetButtonDown(Move1)) {
				GetComponent<Animator>().SetTrigger("AirKick");
				setStatus("AirKick");
				break;
			}

			//AxeKick
			if(Input.GetButtonDown(Move2)) {
				GetComponent<Animator>().SetTrigger("AxeKick");
				setStatus("AxeKickChannel");
				break;
			}

			//Jump
			if(Input.GetAxisRaw(Vertical)==1 && canJump) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,9);
				GetComponent<Animator>().SetTrigger("Jump");
				canJump = false;
				break;
			}
			//Dive
			if(Input.GetAxisRaw(Vertical)==-1) {
				GetComponent<Animator>().SetTrigger("Dive");
				setStatus("Dive");
				canJump = false;
				canDahsMidAir = false;
				break;
			}
			//Idle || Walk
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle") || GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Walk")) {
				setStatus("");
				break;
			}
			//"Stop" Jumping
			if(Input.GetAxisRaw(Vertical)==0 && GetComponent<Rigidbody2D>().velocity.y>5 ) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,5);
			}
			hInput();
			checkDirection();
			break;
		case "Dive":
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,-4);

			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Dive")) {
				setStatus("Land");
				break;
			}
			
			hInput();
			checkDirection();
			break;
		case "Land":
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Land")) {
				setStatus("");
				break;
			}
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "Guard":
			if(!Input.GetButton(Move1)) {
				GetComponent<Animator>().SetBool("Guard", false);
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				setStatus("");
				break;
			}
			InvertedCheckDirection();
			GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.MoveTowards(GetComponent<Rigidbody2D>().velocity.x,0,8*Time.deltaTime),GetComponent<Rigidbody2D>().velocity.y);
			break;
		case "Hit":
			GetComponent<Animator>().SetBool("PowerUpChannel",false);
			GetComponent<Animator>().SetBool("AuraSphereChannel",false);
			if(a!=null) a.Destroy();

			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Hit")) {
				setStatus("");
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				break;
			}
			
			InvertedCheckDirection();
			hitbox.EnableCollider(false);
			break;
		case "Punch1":
			//Get input for the Punch2
			if(Input.GetButtonDown(Move1)) {
				GetComponent<Animator>().SetTrigger("Punch2");
			}
			
			//Punch2
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
				setStatus("Punch2");
				break;
			}
			//""
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch1")) {
				setStatus("");
				GetComponent<Animator>().ResetTrigger("Punch2");
				break;
			}
			
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "Punch2":
			//Get input for...
			if(Input.GetButtonDown(Move1)) {
				if(Input.GetAxisRaw(Horizontal)==0) {
					//...Punch1
					GetComponent<Animator>().SetTrigger("Punch1");
				} else {
					//...Kick
					if( faceRight == (Input.GetAxisRaw(Horizontal)==1) )
						GetComponent<Animator>().SetTrigger("Kick");
				} 
			}
			
			//On exit-time
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
				//Kick
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Kick")) {
					GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x*6,0);
					setStatus("Kick");
					GetComponent<Animator>().ResetTrigger("Punch1");
					break;
				}
				//Punch1
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch1")) {
					setStatus("Punch1");
					GetComponent<Animator>().ResetTrigger("Kick");
					break;	
				}
				//""
				if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Punch2")) {
					setStatus("");
					GetComponent<Animator>().ResetTrigger("Kick");
					GetComponent<Animator>().ResetTrigger("Punch1");
					break;	
				}
			}
			
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "Kick":
			//""
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
				setStatus("");
				break;	
			}
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "AirKick":
			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("AirKick")) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(0,-4);
				setStatus("Dive");
				break;	
			}

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "AxeKickChannel":
			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("AxeKickChannel")) {
				setStatus("AxeKick");
				break;	
			}
			
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		break;
		case "AxeKick":
			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("AxeKick")) {
				setStatus("Land");
				break;	
			}
			
			GetComponent<Rigidbody2D>().velocity = new Vector2(0,-10);
			break;
		case "PowerUpChannel":
			//If the channel is stopped
			if(!Input.GetButton(Move2)) {
				GetComponent<Animator>().SetBool("PowerUpChannel",false);
			}

			//If the power up is ready
			if(Time.time-pTime>=2) {
				pTime = Time.time;
				GetComponent<Animator>().SetBool("PowerUp",true);
				GetComponent<Animator>().SetBool("PowerUpChannel",false);
			}

			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("PowerUpChannel")) {
				setStatus("");
				break;	
			}
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "AuraSphereChannel":

			//Increase Aurasphere size
			a.transform.localScale = new Vector3(0.2f+(Time.time-aTime)*0.4f,0.2f+(Time.time-aTime)*0.4f,1);

			//If the channel is stopped
			if(!Input.GetButton(Move2)) {
				GetComponent<Animator>().SetBool("AuraSphereChannel",false);
			}

			//If the aurasphere up is ready
			if(Time.time-aTime>=2) {
				GetComponent<Animator>().SetBool("AuraSphereChannel",false);
			}

			//On exit
			if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("AuraSphereChannel")) {
				//Throw the Aurasphere
				if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("AuraSphere")) {
					setStatus("AuraSphere");
					GetComponent<Animator>().SetBool("AuraSphereChannel",false);
					a.pokemon = this;
					a.setHit(Mathf.FloorToInt(5+(Time.time-aTime)*11),Mathf.FloorToInt(1+(Time.time-aTime)),Mathf.FloorToInt(1+(Time.time-aTime)));
					a.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x * (6+Time.time-aTime),0);
					a.GetComponent<SpriteRenderer>().color = Color.white;
					a.GetComponent<CircleCollider2D>().enabled = true;
					a = null;
				} else {
					a.Destroy();
				}
				break;
			}
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		case "AuraSphere":
			//On exit
			if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
				setStatus("");
				break;
			}
			
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			break;
		}
	}

	public override void Death () {
		GetComponent<Animator>().SetBool("PowerUp",false);
		base.Death ();
	}
}
