﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIController : MonoBehaviour {
	
	public Pokemon pokemon;
	public Text percText;
	public Text lifesText;

	void Update () {
		percText.text = pokemon.perc + "%";
		percText.fontSize = Mathf.CeilToInt(Mathf.Min(24, 15+pokemon.perc/20));
		lifesText.text = "x" + Mathf.Max(0,pokemon.lifes);
	}
}
