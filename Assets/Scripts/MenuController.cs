﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {
	public GameObject Game;
	public GameObject Map;
	public GameObject Player1;
	public GameObject Player2;

	public RectTransform Cursor1;
	public RectTransform Cursor2;
	public float CursorSpeed;

	public RectTransform[] pokemonButton;

	public GameObject[] PlayerBars;
	public GameObject[] PlayerSelections;

	public GameObject PlayButton;

	public GameObject[] PrefabPokemonBars;
	public GameObject PrefabGame;
	public GameObject PrefabMap;

	void Start() {
		DontDestroyOnLoad(this);
	}

	void Update () {
		CheckButtons();
		MoveCursors();
	}

	void CheckButtons() {

		if(PlayerSelections[0].name!="MegaBallSelected" &&
		   PlayerSelections[1].name!="UltraBallSelected") {
			PlayButton.GetComponent<Animator>().SetBool("Enabled", true);
		} else PlayButton.GetComponent<Animator>().SetBool("Enabled", false);

		for(int i=0; i<pokemonButton.Length; i++) {
			if(pokemonButton[i].GetComponent<BoxCollider2D>().OverlapPoint(Cursor1.position) ||
			   pokemonButton[i].GetComponent<BoxCollider2D>().OverlapPoint(Cursor2.position) )
				pokemonButton[i].GetComponentInChildren<Animator>().SetBool("Highlighted", true);
			else pokemonButton[i].GetComponentInChildren<Animator>().SetBool("Highlighted", false);
		}
	}

	void MoveCursors() {
		//Move the cursors
		if(!Cursor1.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Roll"))
			Cursor1.position += new Vector3(Input.GetAxisRaw("Horizontal_1") / CursorSpeed, Input.GetAxisRaw("Vertical_1") / CursorSpeed);
		if(!Cursor2.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Roll"))
			Cursor2.position += new Vector3(Input.GetAxisRaw("Horizontal_2") / CursorSpeed, Input.GetAxisRaw("Vertical_2") / CursorSpeed);

		//Inputs
		if(Input.GetButtonDown("Fire1_1")) {
			Cursor1.GetComponent<Animator>().SetTrigger("Roll");
			OnClick(Cursor1, 0);

		}
		if(Input.GetButtonDown("Fire1_2")) {
			Cursor2.GetComponent<Animator>().SetTrigger("Roll");
			OnClick(Cursor2, 1);
		}

		//Boundaries
		Cursor1.position = new Vector3( Mathf.Clamp(Cursor1.position.x, -6.5f, 6.5f),
		                                Mathf.Clamp(Cursor1.position.y, -3.5f, 3.5f),
		                                Cursor1.position.z);

		Cursor2.position = new Vector3( Mathf.Clamp(Cursor2.position.x, -6.5f, 6.5f),
		                                Mathf.Clamp(Cursor2.position.y, -3.5f, 3.5f),
		                                Cursor2.position.z);

	}

	void OnClick(RectTransform cursor, int c) {
		for(int i=0; i<pokemonButton.Length; i++) {
			if(pokemonButton[i].GetComponent<BoxCollider2D>().OverlapPoint(cursor.position)) {
				Destroy(PlayerSelections[c]);
				PlayerSelections[c] = Instantiate(PrefabPokemonBars[i]);
				PlayerSelections[c].transform.SetParent(PlayerBars[c].transform, false);
				break;
			}
		}
		if(PlayButton.GetComponent<BoxCollider2D>().OverlapPoint(cursor.position)) {
			Application.LoadLevel("GameScene");
		}
	}

	//Set up the game scene after it is loaded
	void OnLevelWasLoaded(int level) {
		if(level == 1) {
			Map = Instantiate(PrefabMap);
			Game = Instantiate(PrefabGame);
			Game.GetComponentInChildren<CameraController>().Menu = gameObject;
			gameObject.SetActive(false);
		}
	}

}
