﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject Game;
	public GameObject Menu;
	public GUIController[] PlayersGUI;

	public GameObject[] players;
	private Transform[] transforms;

	public GameObject[] PrefabPokemon;
	public GameObject[] PrefabPokemonInGameGUI;

	public float minSize;
	public float maxSize;
	public float dampTime;
	public float zoomSpeed;
	public Vector3 moveSpeed;
	public float edgeBuffer;

	private Vector3 targetPosition;

	void Start() {
		transforms = new Transform[players.Length];

		for(int i=0; i<players.Length; i++) {
			players[i] = Instantiate(PrefabPokemon[Menu.GetComponent<MenuController>().PlayerSelections[i].name[0]-48]);
			players[i].transform.position = new Vector2(-2+i*4,0);
			players[i].transform.SetParent(Game.transform, false);
			players[i].GetComponent<Pokemon>().setButtons(i);

			PlayersGUI[i].pokemon = players[i].GetComponent<Pokemon>();
			Instantiate(PrefabPokemonInGameGUI[Menu.GetComponent<MenuController>().PlayerSelections[i].name[0]-48]).transform.SetParent(PlayersGUI[i].transform,false);
			transforms[i] = players[i].transform;
		}
	}

	void Update () {
		Move();
		Zoom();
		Edge();
	}

	private void Move() {
		AveragePosition();
		transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref moveSpeed, dampTime);
	}

	private void AveragePosition() {
		targetPosition = Vector2.zero;
		
		for(int i = 0; i < transforms.Length; i++)
			targetPosition += transforms[i].position;

		targetPosition /= transforms.Length;
		
		targetPosition.z = transform.position.z;
	}

	private void Zoom() {
		GetComponent<Camera>().orthographicSize = Mathf.SmoothDamp(GetComponent<Camera>().orthographicSize, RequiredSize(), ref zoomSpeed, dampTime);
	}

	private void Edge() {
		//Right edge
		if(-GetComponent<Camera>().orthographicSize*GetComponent<Camera>().aspect + transform.position.x < -11)
			transform.position = new Vector3( -11 + GetComponent<Camera>().orthographicSize*GetComponent<Camera>().aspect,
			                                 transform.position.y,
			                                 transform.position.z);
		//Left edge
		if(GetComponent<Camera>().orthographicSize*GetComponent<Camera>().aspect + transform.position.x > 13)
			transform.position = new Vector3( 13 - GetComponent<Camera>().orthographicSize*GetComponent<Camera>().aspect,
			                                 transform.position.y,
			                                 transform.position.z);

		//Top edge
		if(GetComponent<Camera>().orthographicSize + transform.position.y > 7)
			transform.position = new Vector3( transform.position.x,
			                                 7 - GetComponent<Camera>().orthographicSize,
			                                 transform.position.z);
		//Bottom edge
		if(-GetComponent<Camera>().orthographicSize + transform.position.y < -6)
			transform.position = new Vector3( transform.position.x,
			                                 -6 + GetComponent<Camera>().orthographicSize,
			                                 transform.position.z);
	}

	private float RequiredSize() {
		Vector2 desiredLocalPos = transform.InverseTransformPoint(targetPosition);
		
		float size = 0f;
		
		for (int i = 0; i < transforms.Length; i++) {
			
			Vector2 targetLocalPos = transform.InverseTransformPoint(transforms[i].position);
			
			Vector2 desiredPosToTarget = targetLocalPos - desiredLocalPos;
			
			size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.y));
			
			size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.x) / GetComponent<Camera>().aspect);
		}
		
		size += edgeBuffer;
	
		size = Mathf.Clamp(size,minSize,maxSize);
		
		return size;
	}
}
