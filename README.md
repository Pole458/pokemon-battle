# Pokémon Battle #

Hi! Welcome to Pokemon Battle!

This is a little Pokémon brawler game realized with Unity. It can be played by two people on the same computer.

If you want to play you can download it [here](https://bitbucket.org/Pole458/pokemon-battle/downloads/Pokemon%20Battle.zip).

You can also find a gameplay video here:

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/QrKSG8lQanY/0.jpg)](http://www.youtube.com/watch?v=QrKSG8lQanY)


## How to play ##

You can play one betweeen Zangoose, Snivy and Lucario. In order to win, you have to knock the other player out of the stage for three times.

## Menu ##

Player1: To choose your Pokèmon, move the cursor with **wasd** and select it with **G**

Player2: To choose your Pokèmon, move the cursor with the **arrow keys** and select it with **numpad1**

**command** | **Player 1** | **Player 2**
------------|--------------|-------------
movement | wasd | arrow keys
Button 1 | G | numpad1
Button 2 | H | numpad2


### Zangoose ###
**buttons** | **attack**
------------|-----------
1 |	Punches
1 mid-air | Jump Attack
1 & down | Guard
1 + 1 + 1 & right/left | 2 Punches + Shoulder Dash
1 + 1 + 1 & down | 2 Punches + Low Attack
Hold 2 | Charges Dash
Hold 2 & down |	Charges Hone Claws

### Snivy ###
**buttons** | **attack**
------------|-----------
1 | Punches
1 mid-air | Jump Vinewhip
1 & down | Guard
1 + 1 + 1 & right/left | 2 Punches + Vinewhip
2 | Razor Leaf
Spam 2 mid-air | Leaf Tornado
Hold 2 & down | Charges Solar Beam

### Lucario ###
**buttons** | **attack**
------------|-----------
1 | Punches
1 mid-air | Air Kick
1 & down | Guard
1 + 1 + 1 & right/left | 2 Punches + Kick
2 mid-air | Axe Kick
Hold 2 | Charges Power Up
Hold 2 & down | Charges Aurasphere


## Credits ##
Programmed by Pole.

Sprites made by Neoweegee. You can find more of his works at https://neoweegee.deviantart.com/